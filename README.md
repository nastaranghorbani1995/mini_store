# MINISTORE

## Table of Contents

- [MINISTORE](#ministore)
	- [Table of Contents](#table-of-contents)
	- [About](#about)
	- [Getting Started](#getting-started)
		- [Prerequisites](#prerequisites)

## About

This is a small project which was a challenge. a store with product and category and serveral users with different permissions. Implemented with python/django.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development. See [deployment](#deployment) for notes on how to deploy the project on a live system.

you should have tokens directory which is in gitignore, if you want to deploy, tell me to send a tokens.zip and extract it in the root of project.


### Prerequisites

First make an environment in project directory and install requierments.

```
pip install -r requierments.txt
```

And migrate the migrations.


```python
python manage.py migrate
```

