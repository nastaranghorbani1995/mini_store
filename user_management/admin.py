from django.contrib import admin

from user_management.models import CustomUser


@admin.register(CustomUser)
class CustomerAdmin(admin.ModelAdmin):
    pass
