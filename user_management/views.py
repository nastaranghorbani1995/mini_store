from django.contrib.auth import authenticate
from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import status
from django.utils.translation import ugettext as _
from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import check_password, make_password

from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from user_management.models import CustomUser


class LoginView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, *args, **kwargs):
        if True:
            username = request.data.get('username')
            password = request.data.get('password')
            if not username or not password:
                return Response({"status": False, "data": None, "message": _("bad request")},
                                status=status.HTTP_400_BAD_REQUEST)

            user = authenticate(username=username, password=password)

            if user is not None:

                try:
                    user_query = get_user_model().objects.get(username=username)

                except Exception as e:
                    # process_log.error(f'[client: ]-[message: {str(e)} ]')
                    return Response({"status": False, "data": None, "message": _("user not found")}, status=status.HTTP_404_NOT_FOUND)

                # serializer = EmployeeSerializer(user_query)
                user.id = str(user_query.id)

                refresh = TokenObtainPairSerializer.get_token(user)
                # refresh = RefreshToken.for_user(user_obj)

                return Response({'status': True,
                                     "data":
                                         {
                                             'access': str(refresh.access_token),
                                             'refresh': str(refresh),
                                             # 'user': serializer.data
                                         },
                                     "message": _("Success")}, status=status.HTTP_200_OK)

            else:
                # process_log.error(f'[client: ]-[message: forbidden ]')
                return Response({"status": False, "data": None, "message": _("forbidden")}, status=status.HTTP_403_FORBIDDEN)

        # except Exception as e:
        #     # process_log.error(f'[client: ]-[message: {str(e)} ]')
        #     return Response({"status": False, "data": None, "message": _("forbidden")},
        #                             status=status.HTTP_404_NOT_FOUND)
