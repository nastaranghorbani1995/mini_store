from django.urls import path
from rest_framework_simplejwt.views import (
    TokenRefreshView,
)
from user_management.views import LoginView

urlpatterns = [
    path('login', LoginView.as_view()),
    path('refresh-token', TokenRefreshView.as_view()),

]
