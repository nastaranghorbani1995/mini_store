from django.db import models
from django.utils.translation import gettext as _

from user_management.models import CustomUser


class Category(models.Model):
    name = models.CharField(_("name"), max_length=50)
    parent = models.ForeignKey(
        "self", null=True, blank=True, related_name="children", on_delete=models.CASCADE
    )

    class Meta:
        verbose_name_plural = "categories"

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        # prevent a category to be itself parent
        if self.id and self.parent and self.id == self.parent.id:
            self.parent = None
        super().save(*args, **kwargs)


class Product(models.Model):
    categories = models.ManyToManyField(Category, related_name="products", blank=True)
    name = models.CharField(_("name"), max_length=50)
    description = models.CharField(
        _("description"), max_length=255, null=False, blank=True
    )
    price = models.PositiveIntegerField(_("price"), default=0)
    current_sales_number = models.PositiveIntegerField(
        _("current sales number"), default=0
    )
    inventory_status = models.PositiveIntegerField(_("inventory status"), default=0)
    submit_by_user = models.ForeignKey(
        CustomUser, related_name="user", on_delete=models.CASCADE
    )
    date_added = models.DateTimeField(_("added date"), auto_now_add=True)

    class Meta:
        verbose_name_plural = "products"

    def __str__(self):
        return self.name
