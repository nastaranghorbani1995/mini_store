import math


def pagination(page_number, obj, limit_page):
    """
    :param page_number: int
    :param obj: db records
    :param limit_page: int
    :return: objects, total_row, total_page
    """
    try:
        page_number = int(page_number)

        if page_number < 1:
            page_number = 1

    except Exception as e:
        page_number = 1
        pass

    limit_page = int(limit_page)
    offset = (page_number - 1) * limit_page

    total_row = obj.count()
    total_page = int(math.ceil(total_row / limit_page))

    objects = obj[offset : offset + limit_page]
    return objects, total_row, total_page
