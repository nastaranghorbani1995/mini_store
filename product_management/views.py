from django.utils.translation import ugettext as _

from rest_framework.decorators import api_view, throttle_classes, permission_classes
from rest_framework.throttling import UserRateThrottle
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import status
from django.contrib.auth import get_user_model

from django.db.models import Q

from product_management.models import Product
from product_management.serializers import ProductSerializer
from product_management.utils.product_utils import pagination


@throttle_classes([UserRateThrottle])
@api_view(["GET"])
@permission_classes([AllowAny])
def product_list(request):
    try:
        search = request.GET.get("search", "")
        category_id = request.GET.get("category_id", None)
        page_number = request.GET.get("page_number", 1)

        product = Product.objects.all()

        if search:
            product = Product.objects.filter(
                Q(name__contains=search) | Q(inventory_status__contains=search)
            )

        if category_id:
            product = product.filter(categories__id=category_id)

        limit_page = request.GET.get("limit_page", None)
        if not limit_page:
            user_id = request.user.id
            try:
                user = get_user_model().objects.get(id=user_id)
                if user.is_superuser:
                    limit_page = 1000

                else:
                    limit_page = 100
            except:
                limit_page = 100
        objects, total_row, total_page = pagination(page_number, product, limit_page)

        serializer = ProductSerializer(instance=objects, many=True)

        return Response(
            {
                "status": True,
                "data": {
                    "list_data": serializer.data,
                    "total_row": total_row,
                    "total_page": total_page,
                },
                "message": _("success"),
            },
            status=status.HTTP_200_OK,
        )

    except Exception as e:
        return Response(
            {"status": False, "data": None, "message": str(e)},
            status=status.HTTP_400_BAD_REQUEST,
        )


@throttle_classes([UserRateThrottle])
@api_view(["POST"])
@permission_classes([IsAuthenticated])
def create_product(request):
    user_id = request.user.id
    try:
        user = get_user_model().objects.get(id=user_id)
        if user.is_staff:
            name = request.data.get("name", None)
            if not name:
                return Response(
                    {"status": False, "data": None, "message": _("name is empty")},
                    status=status.HTTP_400_BAD_REQUEST,
                )

            description = request.data.get("description", None)
            if not description:
                return Response(
                    {
                        "status": False,
                        "data": None,
                        "message": _("description is empty"),
                    },
                    status=status.HTTP_400_BAD_REQUEST,
                )

            price = request.data.get("price", None)
            if not price:
                return Response(
                    {"status": False, "data": None, "message": _("price is empty")},
                    status=status.HTTP_400_BAD_REQUEST,
                )

            categories = request.data.get("categories", None)
            if not categories:
                return Response(
                    {
                        "status": False,
                        "data": None,
                        "message": _("categories is empty"),
                    },
                    status=status.HTTP_400_BAD_REQUEST,
                )

            product = Product.objects.create(
                name=name,
                description=description,
                price=price,
                submit_by_user=user,
            )
            product.categories.set(categories)

            export_data = {
                "id": product.id,
                "name": product.name,
                "submit_by_user": product.submit_by_user.username,
            }
            return Response(
                {"status": True, "data": export_data, "message": _("success")},
                status=status.HTTP_201_CREATED,
            )
        else:
            return Response(
                {"status": False, "data": None, "message": _("authorization failed")},
                status=status.HTTP_401_UNAUTHORIZED,
            )
    except Exception as e:
        return Response(
            {"status": False, "data": None, "message": str(e)},
            status=status.HTTP_400_BAD_REQUEST,
        )


@throttle_classes([UserRateThrottle])
@api_view(["DELETE"])
@permission_classes([IsAuthenticated])
def delete_product(request, product_id):
    try:
        user_id = request.user.id
        product = Product.objects.get(id=product_id)
        product_submiter_id, current_sales_number = (
            product.submit_by_user,
            product.current_sales_number,
        )
        user = get_user_model().objects.get(id=user_id)
        if user.is_superuser or user_id == product_submiter_id:
            if current_sales_number == 0:
                product.delete()
                return Response(
                    {"status": True, "data": None, "message": "success"},
                    status=status.HTTP_204_NO_CONTENT,
                )
            else:
                return Response(
                    {"status": False, "data": None, "message": "have product in store"},
                    status=status.HTTP_400_BAD_REQUEST,
                )
        else:
            return Response(
                {"status": False, "data": None, "message": "forbidden"},
                status=status.HTTP_403_FORBIDDEN,
            )
    except Exception as e:
        return Response(
            {"status": False, "data": None, "message": str(e)},
            status=status.HTTP_400_BAD_REQUEST,
        )
