from django.urls import path

from product_management.views import product_list, create_product, delete_product

urlpatterns = [
    path('product-list', product_list),
    path("create-product", create_product),
    path('delete-product/<str:product_id>', delete_product),
]